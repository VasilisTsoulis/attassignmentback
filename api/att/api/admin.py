from django.contrib import admin
from att.api.models import Text

@admin.register(Text)
class TextAdmin(admin.ModelAdmin):
    fields = ('urltext',)
    list_display = ['urltext']
    search_fields = ('urltext',)
