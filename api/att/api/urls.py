from django.conf.urls import url, include
from rest_framework import routers
from att.api import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'textuser', views.TextViewSet)

urlpatterns = [
    url(r'^', include(router.urls))
]