from django.contrib.auth.models import User, Group
from rest_framework import serializers
from att.api.models import Text

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email')

class TextSerializer(serializers.ModelSerializer):
    class Meta:
        model = Text
        fields = ('urltext',)
